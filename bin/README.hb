
Il y a des erreurs que je n'ai pas réussi à corriger avec intellidg
J'ai donc suivi la démarche suivante :
Afin de pouvoir développer la classe BadgeWalletDao, j'ai développé cette classe dans Exercice4 en gardant la
 structure de fichier initiale.
 Le test initial dans exercice4/src/dao/test a fonctionné sous Eclipse.
 J'avais cependant une petite difference en fin de fichier et j'ai tronqué les derniers caracteres de encodedimage pour obtenir
 légalité.
 Mais je n'ai pas su introduire un module maven sous Eclipse.

 J'ai ensuite essayé de créer ce module Maven en utilisant Intellij.
 J'ai ensuite développé badgeWalletDao (copier/coller d'aprés ce que j'avais fait sous Eclipse)
 Ce faisant je n'ai pas réussi à compiler sous Intellij.

