
package fr.cnam.foad.nfa035.dao;
//import java.io.*;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageFileFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;

import java.io.*;
/**
 * Class  badgeWalletDao gathers method to manage badges (add , get)
 *
 * @param badge
 *
 *
 */
public class badgeWalletDao {

        File badgeWallet;
        public void BadgeWalletDAO(String badgeWalletName) {badgeWallet = new File(badgeWalletName);}
        /**
         * Ajouter un badge
         * @param badge : badge � ajouter dans le portefeuille.
         * @throws IOException
         */
        public void addBadge(File badge) throws IOException

        {
            // File image = new File(badge);
            ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());

            // Sérialisation
            ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
            serializer.serialize(badge, media);

            String encodedImage = media.getEncodedImageOutput().toString();
            FileOutputStream fos = new FileOutputStream(badgeWallet);
            fos.write(media.getEncodedImageOutput().toString().getBytes());
            fos.close();

            /* ajouter encodedImage dans le fichier badgeWallet */
        }

    /**
     * get a badge as ByteArrayOutputStream
     * @param badge
     * @throws IOException
     */
        public void getBadge(ByteArrayOutputStream badge) throws IOException
        {
            ImageFileFrame media = new ImageFileFrame(badgeWallet);
            ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
            ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);
            deserializer.deserialize(media);
            byte[] deserializedImage = ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
            badge.writeBytes(deserializedImage);


        }
        /**
         * get a badge as OutputStream
         *
         * @param badge
         *
         * @throws IOException
        */
        public void getBadge(OutputStream badge) throws IOException
        {
            ImageFileFrame media = new ImageFileFrame(badgeWallet);
            ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
            ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);
            deserializer.deserialize(media);
            byte[] deserializedImage = ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
            badge.write(deserializedImage);


        }
}
